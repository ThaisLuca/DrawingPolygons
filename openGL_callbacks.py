#!/usr/bin/env python
# -*- coding: utf-8 -*-
#
## @package openGL_callbacks
#
#  Callbacks used by OpenGL, like a resize function, mouse function, mouse drag functions. 
#  Also some other functions called by OpenGL functions.
#
#  @author Thais Luca
#  @date 11/2017 
#

import OpenGL 

from OpenGL.GL import *
from OpenGL.GLUT import *
from OpenGL.GLU import *
from math import sqrt

from mathfunctions import *
from geometry import *
from tessellator import *
from matrix import *
import sys, numpy as np, random

## Screen width and height
WIDTH = 640
HEIGHT = 480

## Flags to control drags and clicks
click = False
hit = False
polyClicked = None
dragStart = None
dragX = None
dragY = None
dragging = False

## The color gray for coloring the nails.
gray = [0.268088277223, 0.284165006733, 0.389311845093]

## Points used while drawing a polygon using mouse.
p1 = None
p2 = None

## Dictionaries to control polygons relations.
lines = []
points = []
polygons = []
nails = []
sons = {}
articulations = {}
nailsOfPolygon = {}
nailsOfChildren = {}
nailConnector = {}

def mouseFunction(button, state, x, y):
    """Gets the clicks by the user to create the lines and polygons. Right button for a new line or left button for a nail."""	
    global click, p1, p2, lines, points, hit, polyClicked, dragX, dragY, dragStart, dragging
	
    p = Point(x,y)
    if(button == GLUT_RIGHT_BUTTON and state == GLUT_UP):
	polygonHited = 0
	for nail in nails:
		if(nail.dist(p) < 5):
			nailIndex = nails.index(nail)
			nails.remove(nail)
			killConnections(nailIndex)	
			renderScene()
			return		

	polygonHited = 0
	for poly in polygons:
		if(poly.contains(p)):
			if(polygonHited == 0):
				polygonHited += 1
				indexDad = polygons.index(poly)
			else:
				indexChild = polygons.index(poly)
				if(checkConnections(indexDad, indexChild)):
					raise ValueError("You can't connect them twice.")

				nails.append(p)
				polygons[indexChild].isChild = True
				articulations[indexChild] = len(nails)-1
				nailConnector[len(nails)-1] = [indexDad, indexChild]
				if(indexDad not in sons):
					sons[indexDad] = [indexChild]
				else:
					sons[indexDad].append(indexChild)

				if(indexDad not in nailsOfPolygon):
					nailsOfPolygon[indexDad] = [len(nails)-1]
				else:
					nailsOfPolygon[indexDad].append(len(nails)-1)

				if(indexChild not in nailsOfChildren):
					nailsOfChildren[indexChild] = [len(nails)-1]
				else:
					nailsOfChildren[indexChild].append(len(nails)-1)

				renderScene()
				return

    if(button == GLUT_LEFT_BUTTON and state == GLUT_DOWN and not click):
    	if(len(polygons) != 0):
		for poly in polygons:
			if(poly.contains(p)):
				hit = True
				dragStart = Point(x,y)
 				polyClicked = poly
				dragX = x
				dragY = y

    if(button == GLUT_LEFT_BUTTON and state == GLUT_UP):
	if(dragging):
		dragX = None
		dragY = None
		polyClicked = None
		dragging = False
		hit = False
		return 
	if((dragX == x and dragY == y) or (dragX == None and dragY == None)):

		if(not click):
		    click = True
		    if(p1 == None):
			p1 = Point(x,y)
		    else:
		    	p1.__setitem__(0, x)
		    	p1.__setitem__(1, y)

		    newP1 = Point(x,y)

		    points.append(newP1)

		else:
 		    newP2 = Point(x,y)

		    if(points[0].dist(newP2) < 5):

			color = [random.uniform(0.0,1.0), random.uniform(0.0,1.0), random.uniform(0.0,1.0)]
			if(color[0] == 1.0 and color[1] == 1.0 and color[2] == 1.0):
				color = [random.uniform(0.0,1.0), random.uniform(0.0,1.0), random.uniform(0.0,1.0)]
			poly = ModifiedPolygon(points, color, False)
			polygons.append(poly)
			click = False
			lines = []
			points = []
	
			dragX = None
			dragY = None
			p2 = None
			renderScene()

		    else:
		    	newP2 = Point(x,y)
			newline = Line(points[-1],newP2)
			for line in lines:
				if(properIntersect(newline, line)):
					raise ValueError("No self-intersections allowed.")
			
			points.append(newP2)
			lines.append(newline)
		
			dragX = None
			dragY = None

			renderScene()

			p1.__setitem__(0,x)
			p1.__setitem__(1,y)


def mouseDrag(x, y):
    """Gets mouse motion to draw lines."""	
    global p2, dragX, dragY, dragStart    

    if(click):
	if(p2 == None):
		p2 = Point(x, y)
	else:
        	p2.__setitem__(0, x)
        	p2.__setitem__(1, y)

	dragX = None
	dragY = None

    	renderScene()

def mouseDragPolygon(x,y):
   """Gets mouse motion to drag and drop or rotate polygons."""	
   global dragStart, dragX, dragY, dragging

   if(click):
	return
   if(hit and not polyClicked.isChild):
	dx = getDistance(dragStart.x, x)
	dy = getDistance(dragStart.y, y)

	index = polygons.index(polyClicked)

	setPolygonsNewPosition(index, dx, dy)

	dragStart.x = x
	dragStart.y = y
	dragging = True
	
	if(index in sons):
		loopAndSetPositions(index, dx, dy)

	if(index in nailsOfPolygon):
		setNailsPosition(index, dx, dy)
		
   if(hit and polyClicked.isChild):
	dragging = True
	i = articulations[polygons.index(polyClicked)]
	axis = Point(nails[i].x, nails[i].y, nails[i].z)
	j = Point(x, y, 0.0)
	angle = findAngle(dragStart, axis, j)
		
	index = polygons.index(polyClicked)
	matrix = translateAndRotate(angle, axis, Point(0, 0, 1))
	applyTransformation(polyClicked, matrix)

	if(index in sons):
		applyTransformationToKids(index, matrix)

   dragX = None
   dragY = None

   renderScene()

def renderScene():
    """Draws the scene"""

    global p1, p2, polygons

    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT)
    glMatrixMode(GL_PROJECTION)

    for polygon in polygons:
        colors = polygon.__getcolor__()
    	glColor3f(colors[0], colors[1], colors[2])

	concave = tessellate(polygon)
	glCallList(concave)
	    
    drawNails()
    if(click):
	drawTemporaryLines()
    glutSwapBuffers()
    glutPostRedisplay()
    glFlush()

def drawNails():
    """Function to draw nails."""	

    glColor3f(gray[0], gray[1], gray[2])
    glPointSize(10.0)
    glEnable(GL_POINT_SMOOTH)
    glHint(GL_LINE_SMOOTH_HINT, GL_NICEST)
    glBegin(GL_POINTS)
    for nail in nails:
	glVertex3f(nail.x, nail.y , 0)
    glEnd()


def drawTemporaryLines():
    """This function draws the lines before they become a polygon."""	

    glColor3f(0.0, 0.0, 0.0)

    glLineWidth(2.0)
    glBegin(GL_LINES)
    for line in lines:
        glVertex3f(line.p1.x, line.p1.y, 0.0)
        glVertex3f(line.p2.x, line.p2.y, 0.0)
    glEnd()

    if(p1 != None and p2 != None):
        glLineWidth(2.0)
        glBegin(GL_LINES)
        glVertex3f(p1.x, p1.y, 0.0)
        glVertex3f(p2.x, p2.y, 0.0)
        glEnd()

def resizeScene(width, height):
        """Resizes the screen."""

	WIDTH = width
	HEIGHT = height
	glViewport(0, 0, WIDTH, HEIGHT);
	glMatrixMode(GL_PROJECTION);
	glLoadIdentity();

	glOrtho(0.0, WIDTH, HEIGHT, 0.0, -1.0, 1.0)
	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();
	return

def setPolygonsNewPosition(index, dx, dy):
    """ Iterate through all polygons vertices to set its new position. """	

    points = polygons[index].points
    for point in points:
        point.x = setNewPosition(point.x, dx)
        point.y = setNewPosition(point.y, dy)
    polygons[index].__setitem__(points)

def applyTransformation(polygon, matrix):
    """This function is used while rotating a polygon to give its vertices and nail a new position."""	

    global articulations
    matrix = np.array(matrix)

    for p in polygon.points:
        point = np.array([p.x, p.y, 0, 1])
        result = matrix.dot(point)
        p.x = result[0]
        p.y = result[1]

    i = polygons.index(polygon)
    for n in nailsOfChildren[i]:
        point = np.array([nails[n].x, nails[n].y, 0, 1])
        result = matrix.dot(point)
        nails[n].x = result[0]
        nails[n].y = result[1]

def applyTransformationToKids(i, matrix):
    """Iterates through all polygons attached to the dad."""	

    for i in sons[i]:
        applyTransformation(polygons[i], matrix)
        if i in sons:
            applyTransformationToKids(i, matrix)

def killConnections(index):
    """Used when removing a nail."""	

    indexDad = nailConnector[index][0]
    indexChild = nailConnector[index][1]
    del nailConnector[index]
    polygons[indexChild].isChild = False
    sons[indexDad].remove(indexChild)
    articulations[indexChild] = None
    nailsOfPolygon[indexDad].remove(index)
    nailsOfChildren[indexChild].remove(index)

def loopAndSetPositions(index, dx, dy):
    """Iterates through a polygon and its sons for setting position."""	

    for i in sons[index]:
        setPolygonsNewPosition(i, dx, dy)

        if(i in nailsOfPolygon):
            setNailsPosition(i, dx, dy)
        if(i in sons):
            loopAndSetPositions(i, dx, dy)

def setNailsPosition(index, dx, dy):
    """Sets the position of a nail."""	

    for i in nailsOfPolygon[index]:
        nails[i].x += dx
        nails[i].y += dy

def deletePolygon():
    """Deletes the current polygon being drawned."""	

    global lines, points, p1, p2, click

    print("Your polygon has been deleted.")
    click = False
    lines = []
    points = []
    p1 = None
    p2 = None
    dragX = None
    dragY = None

    renderScene()
    return

def checkConnections(dad, child):
    """Checks if there's a connection between two polygons"""

    if(dad in sons):
	for i in sons[dad]:
		if(child == i):
			return True
    return False

##############################################################################################################################################

class ModifiedPolygon(Polygon):
    """Class extending Polygon in geometry.py to describe a polygon."""	

    def __init__(self, points, color, isChild):
        """Constructor. Throws an exception if less than three points are given."""

        if len(points) < 3:
            raise ValueError("Polygon must have at least three vertices.")

        ## number of vertices
        self.n = len(points)
        ## list of indexes of vertices
        self.points = points
        ## normal vector of the given polygon
        self.normal = self.compNormal().normalize()
        ## vector of rgb components
        self.color = color
        ## bool to check if a polygon is a child
        self.isChild = isChild

    def __setitem__(self, points):
        self.points = points	

    def __getcolor__(self):
        return self.color
