#!/usr/bin/env python
# -*- coding: utf-8 -*-
#
## @package tessellator
#
#  Draw a polygon by tessellator.
#
#  @author Thais Luca
#  @date 11/2017
#


from OpenGL.GL import *
from OpenGL.GLU import *
from OpenGL.GLUT import *

aux = []

def tessErrorCB(errorCode):
    """ Tess error """
    errorStr = gluErrorString(errorCode)
    print("[ERROR]: " + errorStr)

def tessellate(polygon):
    """Function called for tesselate, draw polygons."""

    global aux

    Id = glGenLists(1)
    if not Id:
        return Id
    tess = gluNewTess()
    if not tess:
        return 0
    gluTessCallback(tess, GLU_TESS_BEGIN, glBegin)
    gluTessCallback(tess, GLU_TESS_END, glEnd)
    gluTessCallback(tess, GLU_TESS_ERROR, tessErrorCB)
    #gluTessCallback(tess, GLU_TESS_COMBINE, glCombine)
    gluTessCallback(tess, GLU_TESS_VERTEX, glVertex3dv)

    colors = polygon.color
	
    glNewList(Id, GL_COMPILE)
    glColor3f(colors[0], colors[1], colors[2])
    gluTessBeginPolygon(tess, 0)         
    gluTessBeginContour(tess)
    for vertex in polygon.points:
        aux = [vertex.x, vertex.y, vertex.z]
        gluTessVertex(tess, aux, aux)
    gluTessEndContour(tess)
    gluTessEndPolygon(tess)
    glEndList()
    gluDeleteTess(tess)

    return Id
