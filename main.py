#!/usr/bin/env python
# -*- coding: utf-8 -*-
#
## @package main
#
#  Classes and geometric utilities commonly used in computational geometry, such as: 
#  point, line, polygon, triangle and box.
#
#  @author Thais Luca
#  @date 10/2017 
#
import OpenGL 

from OpenGL.GL import *
from OpenGL.GLUT import *
from OpenGL.GLU import *

from openGL_callbacks import *
import sys


WIDTH = 640
HEIGHT = 480

ESCAPE_KEY = '\033'
DELETE_KEY = '\177'

## The function called whenever a key is pressed.
#  Note the use of Python tuples to pass in: (key, x, y)
def keyPressed(*args):
    """If escape is pressed, kill everything."""
 
    key = args[0]
    if key == ESCAPE_KEY:
        sys.exit()
    if key == 'H'or key == 'h':
	printHelp()
    if key == DELETE_KEY:
	deletePolygon()

def printHelp():
	"""If the user press key H for help"""
	print("\n")
	print("---- HELP ----")
	print("\n")
	print("Click anywhere on the screen to start a new polygon. When you finish drawing, just connect the ends.")
	print("You can use multiple lines to draw a polygon, but remember you need at least three non-collinear vertices!")
	print("You can't have self-intersections or holes in your polygons.")
	print("If you give up drawing, press the delete key on your keyboard.")
	print("After drawning, you can move your polygon around the scene. Just drag and drop it wherever you desire.")
	print("Use the right button of your mouse to attach two polygons together (add nail) and create articulations.")
	print("You can't attach the same pair of polygons twice or create cycles.")
	print("Click on the nail to destroy the articulation and free your polygons.")

def main(argv=None):
    """Main function"""

    global window

    if argv is None:
        argv = sys.argv

    # pass arguments to init
    glutInit(argv)

    # Select type of Display mode:   
    #  Double buffer 
    #  RGBA color
    glutInitDisplayMode(GLUT_DEPTH | GLUT_RGBA | GLUT_DOUBLE)
    
    # get a 640 x 480 window 
    glutInitWindowSize(WIDTH, HEIGHT)
    
    # the window starts at the upper left corner of the screen 
    glutInitWindowPosition(0, 0)
    
    window = glutCreateWindow(b"Trabalho 2 - Thais Luca")

    # This Will Clear The Background Color To White
    glClearColor(1.0, 1.0, 1.0, 1.0)
    glMatrixMode(GL_PROJECTION)
    gluOrtho2D(0.0, WIDTH, HEIGHT, 0.0)

    #Clear and set buffers.
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT)

    glutSwapBuffers()

    # GLUT When mouse buttons are clicked in window
    glutMouseFunc(mouseFunction)

    # GLUT When the mouse moves
    glutPassiveMotionFunc(mouseDrag)

    glutMotionFunc(mouseDragPolygon)
    
    # Register the function called when our window is resized.
    glutReshapeFunc(resizeScene)

    # Register the function called when the keyboard is pressed.  
    glutKeyboardFunc(keyPressed)

    # When we are doing nothing, redraw the scene.
    glutIdleFunc(renderScene)

    # Start Event Processing Engine 
    glutMainLoop()

## Print message to console, and kick off the main to get it rolling.
if __name__ == "__main__":
    print ("Hit ESC key to quit.") 
    print ("Hit H key for help.")
    #printHelp()
    sys.exit(main())
