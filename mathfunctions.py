#!/usr/bin/env python
# -*- coding: utf-8 -*-
#
## @package mathfunctions
#
#  Functions to do some math and set variables.
#
#  @author Thais Luca
#  @date 11/2017 
#

from math import sqrt
from matrix import *
import sys, numpy as np, math

polygons = []
sons = {}

#
#
def getDistance(start, end):
	"""Calculates the distance between two given points."""
	return end - start

def setNewPosition(p, dist):
	"""Calculates the new position of a given point."""	
	return p + dist 	

def findAngle(dragStart, axis, point): 
	"""Calculates the angle using the nail, the actual point of mouse and the point we started dragging."""	

	start =  axis - point
        anglestart = dragStart - axis
        innerProd = anglestart.x*start.x + anglestart.y*start.y

        try:
            first = math.hypot(anglestart.x, anglestart.y)
            second = math.hypot(start.x, start.y)

            angle = math.acos(innerProd / (first * second))
        except ValueError:
            return 0
        
        angle = ((angle)*1.5)
        angle = math.copysign(angle, anglestart.crossProd(start).z)        
        angle *= -1

	#start = dragStart - axis
	#anglestart = point - axis
	#try:
		#crossProd = anglestart.x*start.y - start.x*anglestart.y
		#crossProd = crossProd / math.fabs(crossProd)

	#except ValueError:
		#return 0

	#angle = 1.5 * crossProd/math.pi
	#angle *= -1
	
	return angle


def checkConnections(i, j):
	"""Check if two polygons are connected."""	

	if(i in sons):
		for child in sons[i]:
			if(child == j):
				return True
	return False

def properIntersect(A, B):
	"""Check if two lines intersect."""	

	if(or2(A.p1, A.p2, B.p1) * or2(A.p1, A.p2, B.p2) < 0 and or2(B.p1, B.p2, A.p1) * or2(B.p1, B.p2, A.p2) < 0):
		return True
	else:
		return False

def or2(p, q, r):
	"""Returns -1 if counter clockwise orientation, +1 if clockwise orientation or 0 if collinear. Based on David Mount's algorithm."""	

	det = (q.y - p.y) * (r.x - q.x) - (q.x - p.x) * (r.y - q.y);

	if(det == 0):
		return 0
	elif(det > 0):
		return 1
	else:
		return -1


def setPolygonsNewPosition(index, dx, dy):
	"""Iterate through a polygons vertices to set its new positions."""	

	points = polygons[index].points
	for point in points:
		point.x = setNewPosition(point.x, dx)
		point.y = setNewPosition(point.y, dy)
	polygons[index].__setitem__(points)

