var searchData=
[
  ['ccw',['ccw',['../classgeometry_1_1Polygon.html#a6cddf1b0d0a458d3e3977113981907d5',1,'geometry.Polygon.ccw()'],['../namespacegeometry.html#ae73675b9c04b7ba31232371185acf46c',1,'geometry.ccw()']]],
  ['ccw3',['ccw3',['../namespacegeometry.html#af15517777a81d090fd9d2ed768c4f95a',1,'geometry']]],
  ['centre',['centre',['../classgeometry_1_1Box.html#a7ff2ef4e781eed912cefb0da5ec660a5',1,'geometry::Box']]],
  ['checkconnections',['checkConnections',['../namespacemathfunctions.html#a6f461124afdea5cb109b040a2b792e1b',1,'mathfunctions.checkConnections()'],['../namespaceopenGL__callbacks.html#a082f946dd8ef89b9f2bbcd85e7e8a8cc',1,'openGL_callbacks.checkConnections()']]],
  ['close',['close',['../classgeometry_1_1Point.html#a2d6616d27433fbaddbda724f137bba75',1,'geometry.Point.close()'],['../namespacegeometry.html#a356db0fcb8dd398f8013fe6c421e039f',1,'geometry.close()']]],
  ['compnormal',['compNormal',['../classgeometry_1_1Polygon.html#a2a7e9040a408fa1a7d7dd417fa8dd356',1,'geometry::Polygon']]],
  ['contains',['contains',['../classgeometry_1_1Polygon.html#afe9e047b821a97f95459d3739c0efa0f',1,'geometry.Polygon.contains()'],['../classgeometry_1_1Box.html#a9262bd3154f2bab6b0fac79dccea8e99',1,'geometry.Box.contains()']]],
  ['contains2',['contains2',['../classgeometry_1_1Box.html#ab650d620fe16df5a00727e469d3f0bd7',1,'geometry::Box']]],
  ['crossprod',['crossProd',['../classgeometry_1_1Point.html#a79dce5becb6c0295cf8d69c490b8bdb3',1,'geometry::Point']]],
  ['crossprod2d',['crossProd2d',['../classgeometry_1_1Point.html#a662c20ca37f2ba2f0e99d10ebae55ff9',1,'geometry::Point']]]
];
